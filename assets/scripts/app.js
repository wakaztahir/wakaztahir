import Menu from "./modules/Menu";
import Modal from "./modules/Modal";

//Main Menu
let menu = new Menu(document.querySelector("#mobile-menu"));
menu.attach(document.querySelector("#mobile-menu-toggle"));

let menuItems = document.querySelectorAll(".menu-item");
menuItems.forEach(item => {
  item.addEventListener(
    "click",
    () => {
      menu.closeMenu.bind(menu)();
    },
    false
  );
});
//Contact Modal
let modal = new Modal(document.querySelector("#contact-modal"));
modal.attach(document.querySelector("#contact-btn"));
modal.attach(document.querySelector("#contact-modal-toggle"));

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
  anchor.addEventListener("click", function(e) {
    e.preventDefault();

    let x = document.querySelector(this.getAttribute("href"));
    if (x != null) {
      x.scrollIntoView({
        behavior: "smooth"
      });
    }
  });
});
