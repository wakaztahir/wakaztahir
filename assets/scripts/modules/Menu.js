class Menu {
  constructor(menu) {
    this.menu = menu;
    this.menuState = false;
    this.attached = [];
    this.menuHeight = menu.childElementCount * 3 + "rem";
  }
  attach(btn) {
    btn.addEventListener(
      "click",
      () => {
        this.toggleMenu();
      },
      false
    );
    this.attached[this.attached.length] = btn;
  }
  openMenu() {
    this.menu.style.height = this.menuHeight;
    this.menuState = true;
  }
  closeMenu() {
    this.menu.style.height = "0";
    this.menuState = false;
  }
  toggleMenu(val) {
    this.menuState ? this.closeMenu() : this.openMenu();
  }
}
export default Menu;
