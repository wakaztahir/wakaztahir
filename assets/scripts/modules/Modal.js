class Modal {
  constructor(modal) {
    this.modal = modal;
    this.modalState = false;
    this.attached = [];
  }
  attach(btn) {
    btn.addEventListener(
      "click",
      () => {
        this.toggleModal();
      },
      false
    );
    this.attached[this.attached.length] = btn;
  }
  openModal() {
    this.modal.style.display = "flex";
    this.modalState = true;
  }
  closeModal() {
    this.modal.style.display = "none";
    this.modalState = false;
  }
  toggleModal() {
    this.modalState ? this.closeModal() : this.openModal();
  }
}

export default Modal;
