const gulp = require("gulp");

const styles = require("./styles");
const scripts = require("./scripts");

const browserSync = require("browser-sync").create();

function server(){
	browserSync.init({
		server:{
			baseDir:"./build"
		}
	});
}

function pushStyles(){
	 return gulp.src('./build/css/bundle.css')
      .pipe(browserSync.stream());
}
function moveIndex(){
	return gulp.src("./assets/index.html")
	.pipe(gulp.dest("./build/"));
}

function devStyles(){
	return styles();
}

function devScripts(cb){
	scripts();
	cb();
}

function reload(cb){
	browserSync.reload();
	cb();
}

gulp.task("watch",function(){
	server();
	gulp.watch("./assets/*.html",gulp.series(moveIndex,reload));
	gulp.watch("./assets/styles/**/*.scss",gulp.series(devStyles,pushStyles));
	gulp.watch("./assets/scripts/**/*.js",gulp.series(devScripts,reload));
});