let gulp = require("gulp");
let webpack = require("webpack");
let path = require("path");

function scripts(build = "development"){
	webpack({
		mode : build,
		entry:{
			App:"./assets/scripts/app.js"
		},
		output:{
			filename:"bundle.js",
			path:path.resolve(__dirname,"./../build/js")
		}
	},function(error,stats){
		if(error){
			console.log(error);
		}
	});
}

module.exports = scripts;