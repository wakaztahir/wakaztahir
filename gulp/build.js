const gulp = require("gulp");
const del = require("del");
const imagemin = require("gulp-imagemin")

const styles = require("./styles");
const scripts = require("./scripts");

function clean(){
	return del(["./build/index.html","./build/css","./build/js","./build/img"]);
}

function moveIndex(){
	return gulp.src("./assets/index.html")
	.pipe(gulp.dest("./build/"));
}
function images(){
	return gulp.src("./assets/images/**/*")
	.pipe(imagemin())
	.pipe(gulp.dest("./build/img"));
}
function buildStyles(){
	return styles({outputStyle:"compressed"});
}
function buildScripts(cb){
	scripts("production");
	cb();
}

gulp.task("build",gulp.series(clean,moveIndex,images,buildStyles,buildScripts));