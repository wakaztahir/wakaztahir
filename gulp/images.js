let gulp = require("gulp");
let imagemin = require("gulp-imagemin");

function images(){
	return gulp.src("./assets/images/**/*")
	.pipe(imagemin())
	.pipe(gulp.dest("./build/img"));
}

module.exports = images;