let gulp = require("gulp");
let rename = require("gulp-rename");
let sass = require("gulp-sass");

function styles(mode = {}){
	return gulp.src("./assets/styles/app.scss")
	.pipe(sass(mode))
	.on("error",function(err){
		console.log(err.toString());
		this.emit("end");
	})
	.pipe(rename("bundle.css"))
	.pipe(gulp.dest("./build/css"));
};

module.exports = styles;